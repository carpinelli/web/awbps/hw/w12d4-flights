// import path from "path";
import path = require("path");

// const express = require("express");
import type { Express, Request, Response, NextFunction } from "express";
// import express from "express";
import express = require("express");
// import jsxEngine = require("jsx-view-engine");
const jsxEngine: any = require("jsx-view-engine");
// import cors from "cors";

// Ensures environment variables are set before next import statements.
// Runs: 'dotenv.config();', so this only has to be done once at the
// top of the main file. It technically runs the entire file.
import "dotenv/config";
// require("dotenv").config();
import mongoConfig from "./config/mongoDb";
// ANNOTATE.
// const mongoConfig = require("./config/mongodb");
import flightRoutes from "./routes/flightRoutes";
// const flightRoutes = require("./routes/flightRoutes");


const app: Express = express();
const PORT: number = 8081;


app.get("/", (request: Request, response: Response) =>
  {
    response.send(`
                  <div>
                    <h1>Welcome to Flights!</h1>
                    <a href="/flights">Flights</a>
                  </div>
    `);
  }
);

// Set "src" directory as the base directory.
// app.use(express.static(path.join(__dirname, "src")));
app.use(express.urlencoded({ extended: true }));
app.use("/flights", flightRoutes);

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jsx");
app.engine("jsx", jsxEngine());
mongoConfig();


app.listen(PORT, () =>
  {
    console.log(`Listening on http://localhost:${PORT}`);
  }
);

