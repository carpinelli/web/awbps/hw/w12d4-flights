// import mongoose from "mongoose";
import mongoose = require("mongoose");


const mongoConfig = async function()
{
  try
  {
    // '!' is non-null assertion operator.
    const response: typeof mongoose = await mongoose.connect(process.env.MONGO_URL!);
    console.log(`Database connected: ${response.connection.host}`);
  }
  catch (exception: any)
  {
    console.log("MongoDB Error: ", exception.message);
  }
  return null;
};


export default mongoConfig;

