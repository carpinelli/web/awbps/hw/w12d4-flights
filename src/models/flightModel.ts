// const mongoose = require("mongoose");
import type { Document, Model, ObjectId, Schema } from "mongoose";
// import mongoose from "mongoose";
import mongoose = require("mongoose");


interface Destination
{
  airport: string;
  arrival: Date;
  _id: ObjectId;
}

interface Flight
{
  airline: string;
  flightNo: number;
  departs: Date;
  airport: string;
  destinations: Destination[];
  _id: ObjectId;
}


const destinationSchema: Schema<Destination> = new mongoose.Schema<Destination>(
  {
    airport: {
      type: String,
      enum: {
        values: ["AUS", "DAL", "LAX", "SAN", "SEA",],
        message: "{VALUE} is not a supported airport.",
      },
    },
    arrival: {
      type: Date,
    },
  },
  { timestamps: true }
);

const flightSchema: Schema<Flight> = new mongoose.Schema<Flight>(
  {
    airline: {
      type: String,
      enum: {
        values: [
          "American",
          "Southwest",
          "United",
          "AUS",
          "DAL",
          "LAX",
          "SAN",
          "SEA",
        ],
        message: "{VALUE} is not a supported airline.",
      },
    },
    flightNo: {
      type: Number,
      min: 10,
      max: 9999,
    },
    departs: {
      type: Date,
      default: function()
        {
          const oneYearLater = new Date();
          oneYearLater.setFullYear(oneYearLater.getFullYear() + 1)
          return oneYearLater;
        },
    },
    airport: {
      type: String,
      enum: {
        values: ["AUS", "DAL", "LAX", "SAN", "SEA",],
        message: "{VALUE} is not a support airport.",
      },
      default: "SAN",
    },
    destinations: {
      type: [destinationSchema],
    },
  },
  { timestamps: true }
);

// Model<Flight>.
const Flight = mongoose.model<Flight>("flights", flightSchema);
const Destination = mongoose.model<Destination>("destinations", destinationSchema);


export type { Flight };
export default Flight;

