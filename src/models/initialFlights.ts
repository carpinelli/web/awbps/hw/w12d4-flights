const initialFlights = [
  {
    airline:  'American',
    flightNo: '765',
    departs: '2023-01-22',
    airport: 'SEA',
    destinations: [{ airport:'AUS', arrival:'2023-01-22' }]
  },
  {
    airline:  'American',
    flightNo: '564',
    departs: '2025-08-13',
    airport: 'SAN',
    destinations: [{ airport:'DAL', arrival:'2025-08-13' }]
  },
  {
    airline:  'American',
    flightNo: '777',
    departs: '2024-12-18',
    airport: 'AUS',
    destinations: [{ airport:'LAX', arrival:'2024-12-18' }]
  },
  {
    airline:  'American',
    flightNo: '589',
    departs: '2024-11-27',
    airport: 'DAL',
    destinations: [{ airport:'SEA', arrival:'2024-11-27' }]
  },
  {
    airline:  'American',
    flightNo: '321',
    departs: '2028-02-14',
    airport: 'LAX',
    destinations: [{ airport:'SAN', arrival:'2028-02-14' }]
  },
];


export default initialFlights;

