import type { Request, Response, NextFunction } from "express";
// import express from "express";
import express = require("express");
import type { Document, Model } from "mongoose";
// import mongoose from "mongoose";
import mongoose = require("mongoose");

// const Flights = require("../models/flightModel");
import Flights from "../models/flightModel";
import initialFlights from "../models/initialFlights";


type ControllerFunction = (request: Request,
                           response: Response,
                           next?: NextFunction) => Promise<void>;
interface FlightController extends Document
{
  Seed: ControllerFunction;
  Index: ControllerFunction;
  New: ControllerFunction;
  Delete: ControllerFunction;
  Update: ControllerFunction;
  Create: ControllerFunction;
  Edit: ControllerFunction;
  Show: ControllerFunction;
}


const Seed = async function(request: Request, response: Response): Promise<void>
{
  await Flights.deleteMany({});
  await Flights.create(initialFlights);
  response.redirect("/flights");
};

const Index = async function(request: Request, response: Response): Promise<void>
{
  const flights = await Flights.find().sort({ createdAt: 1});
  response.render("flights/Index", { flights });
};

const New = async function(request: Request, response: Response): Promise<void>
{
  response.render("flights/New");
};

const Delete = async function(request: Request, response: Response): Promise<void>
{
  try
  {
    const flight = await Flights.findByIdAndDelete(request.params._id);
    // Handle references here:
  }
  catch (exception: any)
  {
    console.log(exception.message);
    response.status(400).send(exception.message);
  }
  response.redirect("/flights");
};

const Update = async function(request: Request, response: Response): Promise<void>
{
  await Flights.findByIdAndUpdate(request.params._id, request.body);
  response.redirect(`/flights/${request.params._id}`);
};

const Create = async function(request: Request, response: Response): Promise<void>
{
  try
  {
    // request.body.destinations = [{ airport: request.body.airport,
    //                                arrival: request.body.destinationDate }];
    const newFlight = await Flights.create(request.body);
    response.redirect("/flights");
    // const newFlight = new Flight();
    // // Obtain the default date
    // const dt = newFlight.departs;
    // // Format the date for the value attribute of the input
    // const departsDate = dt.toISOString().slice(0, 16);
    // res.render('flights/new', {departsDate});
  }
  catch (exception: any)
  {
    console.log(exception.message);
    response.status(400).send(exception.message);
  }
};

const Edit = async function(request: Request, response: Response): Promise<void>
{
  const flight = await Flights.findById(request.params._id);
  response.render("flights/edit", { flight });
};

const Show = async function(request: Request, response: Response): Promise<void>
{
  // populate replaces the ids with actual documents/objects we can use
  // const post = await Posts.findById(req.params.id).populate('comments')
  const flight = await Flights.findById(request.params._id).sort({ destinations: 1 });
  response.render("flights/Show", { flight });
};


const CreateDestination = async function(request: Request, response: Response): Promise<void>
{
  try
  {
    const newFlight = await Flights.findByIdAndUpdate(request.params._id,
                                                      { $push:
                                                        {
                                                          destinations: request.body 
                                                        }
                                                      }
     );
     response.redirect(`/flights/${request.params._id}`);
  }
  catch (exception: any)
  {
    console.log(exception.message);
    response.status(400).send(exception.message);
  }
};

export default
{
  Seed,
  Index,
  New,
  Delete,
  Update,
  Create,
  Edit,
  Show,
  CreateDestination,
};

