// const express = require("express");
import type { Router } from "express";
// import express from "express";
import express = require("express");

// const flightController = require("../controllers/flightsController");
import flightController from "../controllers/flightController";


const router: Router = express.Router();
router.get("/Seed", flightController.Seed);
router.get("/", flightController.Index);
router.get("/New", flightController.New);
router.delete("/:_id", flightController.Delete);
router.put("/:id/Update", flightController.Update);
router.post("/", flightController.Create);
router.post("/:_id", flightController.CreateDestination);
router.get("/:_id/Edit", flightController.Edit);
router.get("/:_id", flightController.Show);


export default router;

