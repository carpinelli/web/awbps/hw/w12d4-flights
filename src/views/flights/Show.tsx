// import React from "react";
import React = require("react");

import type { Flight } from "../../models/flightModel";


const Show = function({ flight }: { flight: Flight })
{
  const styleSpacing = { margin: "1rem", };
  const styleBold = { fontWeight: "bold", };
  return (
    <div style={styleSpacing}>
      <h1>Flight:</h1>
      <hr />
      <span>
        <span style={styleBold}>Airline: </span>
        {flight.airline}
      </span>
      <p>
        <span style={styleBold}>Flight No. : </span>
        {flight.flightNo}
      </p>
      <p>
        <span style={styleBold}>Departs: </span>
        {flight.departs.toLocaleString()}
      </p>
      <p>
        <span style={styleBold}>Airport: </span>
        {flight.airport}
      </p>
      <hr />
      <p>
        <span style={styleBold}>Destinations: </span>
        {flight.destinations.map((destination) => (
          <p>
            {destination.airport}    {destination.arrival.toLocaleDateString()}
          </p>
          )
        )}
      </p>
      <p><span style={styleBold}>Add Destination: </span></p>
      <form action={`/flights/${flight._id}`} method="POST">
        <ul style={{ listStyle: "none", margin: "0", padding: "0", }}>
          <li>
            <label htmlFor="airport">Airport: </label>
            <select name="airport">
              <option value="AUS">AUS</option>
              <option value="DAL">DAL</option>
              <option value="LAX">LAX</option>
              <option value="SAN">SAN</option>
              <option value="SEA">SEA</option>
            </select>
          </li>
          <li>
            <label htmlFor="arrival">Arrival: </label>
            {/* <input type="datetime-local" id="arrival" name="arrival" /> */}
            <input type="date" id="arrival" name="arrival" />
          </li>
          <li className="submit-button">
            <button type="submit">Add Destination</button>
          </li>
        </ul>
      </form>
      <hr />
      <a href="/flights">Flights</a>
      <hr />
    </div>
  );
};


export default Show;

