// import React from "react";
import type { ReactElement } from "react";
import React = require("react");

import type { Flight } from "../../models/flightModel";


interface FlightArray
{
  flights: Flight[];
}

const Index = function({ flights }: FlightArray): ReactElement
{
  const styleSpacing = { margin: "1rem", };
  const styleBold = { fontWeight: "bold", };
  return (
    <div style={styleSpacing}>
      <h1>Flights:</h1>
      {flights.map((flight) => (
        <div key={flight.flightNo}>
          <hr />
          <span>
            <span style={styleBold}>Airline: </span>
            {flight.airline}
          </span>
          <p>
            <span style={styleBold}>Flight No. : </span>
            {String(flight.flightNo) + "    "}
            <a href={`/flights/${flight._id}`}>Details</a>
          </p>
          <p>
            <span style={styleBold}>Departs: </span>
            {flight.departs.toLocaleString()}
          </p>
          <p>
            <span style={styleBold}>Airport: </span>
            {flight.airport}
          </p>
        </div>
      ))}
      <hr />
      <a href="/flights/New"><button>New</button></a>
      <hr />
    </div>
  );
};


export default Index;

