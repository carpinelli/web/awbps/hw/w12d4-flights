// import React from "react";
import type { ReactElement } from "react";
import React = require("react");

import type { Flight } from "../../models/flightModel";


const New = function(): ReactElement
{
  const styleSpacing = { margin: "1rem", };
  const styleBold = { fontWeight: "bold", };
  return (
    <div style={styleSpacing}>
      <p><span style={styleBold}>Add Flight: </span></p>
      <form action="/flights" method="POST">
        <ul style={{ listStyle: "none", margin: "0", padding: "0", }}>
          <li>
            <label htmlFor="airline">Airline: </label>
            <input type="text" id="airline" name="airline" />
          </li>
          <li>
            <label htmlFor="flightNo">Flight No. : </label>
            <input type="number" id="flightNo" name="flightNo" />
          </li>
          <li>
            <label htmlFor="departs">Departs: </label>
            {/* <textarea id="departs" name="departs"></textarea> */}
            <input type="date" id="departs" name="departs" />
          </li>
          <li>
            <label htmlFor="airport">Airport: </label>
            <select name="airport">
              <option value="AUS">AUS</option>
              <option value="DAL">DAL</option>
              <option value="LAX">LAX</option>
              <option value="SAN">SAN</option>
              <option value="SEA">SEA</option>
            </select>
          </li>
          <p><span style={styleBold}>Destination: </span></p>
          <li>
            <label htmlFor="destinationAirport">Airport: </label>
            <select name="destinationAirport">
              <option value="AUS">AUS</option>
              <option value="DAL">DAL</option>
              <option value="LAX">LAX</option>
              <option value="SAN">SAN</option>
              <option value="SEA">SEA</option>
            </select>
          </li>
          <li>
            <label htmlFor="arrival">Arrival: </label>
            <input type="date" id="arrival" name="arrival" />
          </li>
          <li className="submit-button">
            <button type="submit">Create Flight</button>
          </li>
        </ul>
      </form>
    </div>
  );
};


export default New;

